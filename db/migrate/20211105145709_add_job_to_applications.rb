class AddJobToApplications < ActiveRecord::Migration[6.1]
  def change
    add_reference :applications, :job, index: true, foreign_key: true
  end
end
