class AddFullTimeToJobs < ActiveRecord::Migration[6.1]
  def change
    add_column :jobs, :fullTime, :boolean
  end
end
