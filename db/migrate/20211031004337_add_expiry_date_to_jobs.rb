class AddExpiryDateToJobs < ActiveRecord::Migration[6.1]
  def change
    add_column :jobs, :expiry_date, :date
  end
end
