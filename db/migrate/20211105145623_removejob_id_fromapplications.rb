class RemovejobIdFromapplications < ActiveRecord::Migration[6.1]
  def change
    remove_column :applications, :job_id
  end
end
