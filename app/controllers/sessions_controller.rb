class SessionsController  < ApplicationController 

    def create
        user = User.find_by(username: params[:session][:username].downcase)
        if user && user.authenticate(params[:session][:password])
          session[:role] = user.role
          session[:user_id] = user.id
          flash[:notice] = "Logged in successfully."
          redirect_to  ('/jobs')
        else
          flash.now[:alert] = "There was something wrong with your login details."
          render 'new'
        end
      end
       
      def destroy
        session[:user_id] = nil
        session[:role] = nil
        flash[:notice] = "You have been logged out."
        redirect_to  ('/login')
      end

end