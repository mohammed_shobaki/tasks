Rails.application.routes.draw do

  resources :users
  resources :applications
  resources :jobs
  
  get '/register', to: "users#new"
  get '/my', to: "applications#my"
  root  to: "welcome#index"

  # Session Routes
  get "login", to: "sessions#new"
  post "login", to: "sessions#create"
  get "logout", to: "sessions#destroy"
  
end
